package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"carebox.com/tiny-url/cmd/services/api/endpoints"
	"carebox.com/tiny-url/pkg/accessors/tinyurlaccessor"
	"carebox.com/tiny-url/pkg/accessors/tinyurlaccessor/inmemoryurlaccessor"
	"carebox.com/tiny-url/pkg/accessors/tinyurlaccessor/redistinyurlaccessor"
	"carebox.com/tiny-url/pkg/engines/tinyurlengine/customtinyurlengine"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/redis/go-redis/v9"
)

var env = flag.String("env", ".env", ".env file name")
var useRedis = flag.Bool("useredis", false, "if true, use redis instead of in memory storage.")
var port = flag.String("port", "8080", "the port to run the api on")

type ServiceConfig struct {
	RedisAddress string
}

func main() {
	// Start the server
	flag.Parse()

	fmt.Println("Loading environment variables... ")
	if err := godotenv.Load(*env); err != nil {
		log.Fatal("failed to load environment variables", err)
	}

	// Set the router
	fmt.Printf("Starting API :%s...\n", *port)
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(gin.Logger())

	// Allow all CORS just for this purpose.
	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods:    []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowHeaders:    []string{"Origin", "Content-Type"},
	}))

	serviceConfig := ServiceConfig{
		RedisAddress: os.Getenv("REDIS_ADDRESS"),
		// RedisAddress: "127.0.0.1:6379",
	}

	// Init the engines.
	var tinyURLAccessor tinyurlaccessor.API

	tinyURLAccessor = inmemoryurlaccessor.New()
	if *useRedis {
		fmt.Println("Using Redis as a persistent storage...")
		redisOptions := redis.Options{Addr: serviceConfig.RedisAddress}
		tinyURLAccessor = redistinyurlaccessor.New(redisOptions)
	}
	defer tinyURLAccessor.Disconnect(context.Background())

	tinyURLEngine, err := customtinyurlengine.New(tinyURLAccessor, os.Getenv("TINY_URL_BASE_DOMAIN"))
	if err != nil {
		log.Fatal("failed to create tinyurl engine", err)
	}

	endpoints := endpoints.New(tinyURLEngine)

	// Set the routes
	tinyUrlGroup := router.Group("/tinyurl")
	{
		tinyUrlGroup.GET("/:shortURL", endpoints.GetLongURLEndpoint)
		tinyUrlGroup.POST("", endpoints.GenerateTinyURLEndpoint)
	}

	fmt.Println("Running router!")
	if err := router.Run(":" + *port); err != nil {
		log.Println("Gin router error", err)
		panic(err)
	}
}
