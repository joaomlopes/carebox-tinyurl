package endpoints

import (
	"net/http"

	"carebox.com/tiny-url/pkg/engines/tinyurlengine"
	"carebox.com/tiny-url/pkg/utils"
	"github.com/gin-gonic/gin"
)

func New(
	tinyURLEngine tinyurlengine.API,
) *Endpoint {
	return &Endpoint{
		tinyURLEngine: tinyURLEngine,
	}
}

type Endpoint struct {
	tinyURLEngine tinyurlengine.API
}

// @Summary Get long URL
// @Description Get long URL from a short URL generated previously by the API
// @Param shortURL path string true "shorturl"
// @Success 301 {string} string "Redirects to the long URL"
// @Failure 400 {object} string "shortURL is required"
// @Failure 404 {object} string "URL not found"
// @Failure 500 {object} string "Internal server error"
// @Router /tinyurl/{shortURL} [get]
func (e *Endpoint) GetLongURLEndpoint(c *gin.Context) {
	shortURL := c.Param("shortURL")
	if shortURL == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "shortURL is required"})
		return
	}

	longURL, err := e.tinyURLEngine.GetLongURL(c.Request.Context(), shortURL)
	if err != nil {
		if err == utils.ErrURLNotFound {
			c.JSON(http.StatusNotFound, gin.H{"error": "URL not found"})
			return
		}

		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Redirect(http.StatusMovedPermanently, longURL)
}

// @Summary Generate tiny URL
// @Description Generate a tiny URL from a real, long URL
// @Accept json
// @Produce json
// @Param url body string true "url"
// @Success 200 {object} string "http://tiny.url/shorturl"
// @Failure 400 "URL is required"
// @Failure 500 "Internal server error"
// @Router /tinyurl [post]
func (e *Endpoint) GenerateTinyURLEndpoint(c *gin.Context) {
	var req struct {
		URL string `json:"url"`
	}
	if err := c.BindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	shortURL, err := e.tinyURLEngine.GenerateTinyURL(c.Request.Context(), req.URL)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"shortURL": shortURL})
}
