package customtinyurlengine

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"net/url"

	"carebox.com/tiny-url/pkg/accessors/tinyurlaccessor"
	"carebox.com/tiny-url/pkg/engines/tinyurlengine"
	"carebox.com/tiny-url/pkg/utils"
)

type API struct {
	tinyURLAccessor tinyurlaccessor.API
	baseDomain      *url.URL
}

func New(tinyURLAccessor tinyurlaccessor.API, baseDomain string) (*API, error) {
	baseURL, err := url.Parse(baseDomain)
	if err != nil {
		return nil, err
	}

	return &API{
		tinyURLAccessor: tinyURLAccessor,
		baseDomain:      baseURL,
	}, nil
}

// GenerateTinyURL generates a tiny url from a long url
func (a *API) GenerateTinyURL(ctx context.Context, longURL string) (string, error) {
	// Validate the long url as a valid url
	_, err := url.ParseRequestURI(longURL)
	if err != nil {
		return "", utils.ErrInvalidURL
	}

	// Hash the long url to generate a short url
	hash := sha256.New()
	_, err = hash.Write([]byte(longURL))
	if err != nil {
		return "", err
	}

	shortURL := hex.EncodeToString(hash.Sum(nil))

	// Upsert the long url to the accessor
	err = a.tinyURLAccessor.UpsertLongURL(ctx, shortURL, longURL)
	if err != nil {
		return "", err
	}

	// Join the short url with the base domain
	url := a.baseDomain.JoinPath(shortURL)

	return url.String(), nil
}

// GetLongURL gets the long url from a short url
func (a *API) GetLongURL(ctx context.Context, shortURL string) (string, error) {
	return a.tinyURLAccessor.GetLongURL(ctx, shortURL)
}

var _ tinyurlengine.API = (*API)(nil)
