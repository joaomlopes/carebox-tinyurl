package customtinyurlengine_test

import (
	"context"
	"testing"

	"carebox.com/tiny-url/pkg/accessors/tinyurlaccessor/mocktinyurlaccessor"
	"carebox.com/tiny-url/pkg/engines/tinyurlengine/customtinyurlengine"
	"carebox.com/tiny-url/pkg/utils"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestGenerateTinyURL(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tinyURLAccessor := mocktinyurlaccessor.NewMockAPI(ctrl)

	tinyURLEngine, _ := customtinyurlengine.New(tinyURLAccessor, "https://tinyurl.com")

	t.Run("should succeed", func(t *testing.T) {
		tinyURLAccessor.EXPECT().UpsertLongURL(gomock.Any(), "05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d", "https://google.com").Return(nil)

		url, err := tinyURLEngine.GenerateTinyURL(context.Background(), "https://google.com")

		assert.Nil(t, err)
		assert.Equal(t, url, "https://tinyurl.com/05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d")
	})

	t.Run("should error - invalid url", func(t *testing.T) {
		_, err := tinyURLEngine.GenerateTinyURL(context.Background(), "https//google.com")

		assert.NotNil(t, err)
		assert.Equal(t, err, utils.ErrInvalidURL)
	})

	t.Run("should error - upserting url", func(t *testing.T) {
		tinyURLAccessor.EXPECT().UpsertLongURL(gomock.Any(), "05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d", "https://google.com").Return(assert.AnError)
		_, err := tinyURLEngine.GenerateTinyURL(context.Background(), "https://google.com")

		assert.NotNil(t, err)
	})
}

func TestGetLongURL(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tinyURLAccessor := mocktinyurlaccessor.NewMockAPI(ctrl)

	tinyURLEngine, _ := customtinyurlengine.New(tinyURLAccessor, "https://tinyurl.com")

	t.Run("should succeed", func(t *testing.T) {
		tinyURLAccessor.EXPECT().GetLongURL(gomock.Any(), "05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d").Return("https://google.com", nil)

		url, err := tinyURLEngine.GetLongURL(context.Background(), "05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d")

		assert.Nil(t, err)
		assert.Equal(t, url, "https://google.com")
	})

	t.Run("should error - url not found", func(t *testing.T) {
		tinyURLAccessor.EXPECT().GetLongURL(gomock.Any(), "05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d").Return("", utils.ErrURLNotFound)

		_, err := tinyURLEngine.GetLongURL(context.Background(), "05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d")

		assert.NotNil(t, err)
		assert.Equal(t, err, utils.ErrURLNotFound)
	})

	t.Run("should error - getting url", func(t *testing.T) {
		tinyURLAccessor.EXPECT().GetLongURL(gomock.Any(), "05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d").Return("", assert.AnError)

		_, err := tinyURLEngine.GetLongURL(context.Background(), "05046f26c83e8c88b3ddab2eab63d0d16224ac1e564535fc75cdceee47a0938d")

		assert.NotNil(t, err)
	})
}
