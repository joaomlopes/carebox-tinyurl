package tinyurlengine

import "context"

type API interface {
	GenerateTinyURL(ctx context.Context, longURL string) (string, error)
	GetLongURL(ctx context.Context, shortURL string) (string, error)
}
