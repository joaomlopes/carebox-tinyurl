package inmemoryurlaccessor

import (
	"context"

	"carebox.com/tiny-url/pkg/accessors/tinyurlaccessor"
	"carebox.com/tiny-url/pkg/utils"
)

type API struct {
	URLs map[string]string
}

func New() *API {
	return &API{
		URLs: make(map[string]string),
	}
}

func (a *API) GetLongURL(ctx context.Context, shortURL string) (string, error) {
	longURL, ok := a.URLs[shortURL]
	if !ok {
		return "", utils.ErrURLNotFound
	}

	return longURL, nil
}

func (a *API) UpsertLongURL(ctx context.Context, shortURL string, longURL string) error {
	a.URLs[shortURL] = longURL

	return nil
}

func (a *API) Disconnect(ctx context.Context) error {
	a.URLs = make(map[string]string)

	return nil
}

var _ tinyurlaccessor.API = (*API)(nil)
