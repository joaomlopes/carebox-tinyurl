// mockgen -package=mocktinyurlaccessor -source=pkg/accessors/tinyurlaccessor/iface.go -destination=pkg/accessors/tinyurlaccessor/mocktinyurlaccessor/mock.go
package tinyurlaccessor

import "context"

type API interface {
	GetLongURL(ctx context.Context, shortURL string) (string, error)

	UpsertLongURL(ctx context.Context, shortURL string, longURL string) error

	Disconnect(ctx context.Context) error
}
