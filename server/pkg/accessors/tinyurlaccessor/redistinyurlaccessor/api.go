package redistinyurlaccessor

import (
	"context"

	"carebox.com/tiny-url/pkg/accessors/tinyurlaccessor"
	"carebox.com/tiny-url/pkg/utils"
	"github.com/redis/go-redis/v9"
)

type API struct {
	client redis.Cmdable
}

func New(opts redis.Options) *API {
	return &API{
		client: redis.NewClient(&opts),
	}
}

func (a *API) GetLongURL(ctx context.Context, shortURL string) (string, error) {
	result, err := a.client.Get(ctx, shortURL).Result()
	if err != nil && err == redis.Nil {
		return "", utils.ErrURLNotFound
	}

	return result, err
}

func (a *API) UpsertLongURL(ctx context.Context, shortURL string, longURL string) error {
	return a.client.Set(ctx, shortURL, longURL, 0).Err()
}

func (a *API) Disconnect(ctx context.Context) error {
	return a.client.(*redis.Client).Close()
}

var _ tinyurlaccessor.API = (*API)(nil)
