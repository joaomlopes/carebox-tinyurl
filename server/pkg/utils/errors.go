package utils

import "errors"

var (
	ErrInvalidURL  = errors.New("invalid url")
	ErrURLNotFound = errors.New("url not found")
)
