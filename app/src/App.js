import React, { useState } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const [url, setUrl] = useState("");
  const [shortUrl, setShortUrl] = useState("");
  const [error, setError] = useState(null);

  const handleChange = (e) => {
    setUrl(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(
        "http://localhost:8080/tinyurl",
        {
          url,
        },
        {
          "Content-Type": "application/json",
        }
      )
      .then((response) => {
        console.log(response.data);
        setShortUrl(response.data.shortURL);
        setError(null);
      })
      .catch((error) => {
        console.error(error);
        setError(`Failed to shorten URL: ${error.response.data.error}`);
      });
  };

  return (
    <div className="App">
      <h1>URL Shortener</h1>
      <form onSubmit={handleSubmit}>
        <input type="text" name="url" onChange={handleChange} />
        <button type="submit">Submit</button>
      </form>
      {shortUrl && (
        <code>
          <p>Short URL:</p>
          <a href={shortUrl} target="_blank" rel="noreferrer">
            {shortUrl}
          </a>
        </code>
      )}
      {error && <p>{error}</p>}
    </div>
  );
}

export default App;
