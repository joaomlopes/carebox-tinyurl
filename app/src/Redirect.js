import { useEffect } from "react";
import { useParams } from "react-router-dom";

function Redirect() {
  const { shortURL } = useParams();

  useEffect(() => {
    window.location = `http://localhost:8080/tinyurl/${shortURL}`;
  }, [shortURL]);

  return null;
}

export default Redirect;
