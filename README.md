# Application Name

This application consists of a React frontend and a Golang backend.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed [Node.js](https://nodejs.org/en/download/) and [npm](https://www.npmjs.com/get-npm).
- You have installed [Go](https://golang.org/dl/).

## Installing the React Application

To install the React application, follow these steps:

1. Navigate to the React application directory:

```
cd app
```

2. Install the dependencies:

```
npm install
```

## Running the React Application

To run the React application, use this command:

```
npm start
```

The React application will be running at `http://localhost:3000`.

## Installing the Golang Application

To install the Golang application, follow these steps:

1. Navigate to the Golang application directory:

```
cd server
```

2. Install the dependencies:

```
go get
```

3. Copy the `.env.example` file:

```
cp .env.example .env
```

## Running the Golang Application

To run the Golang application, go to the service folder:

```
cd cmd/services/api
```

and use this command:

```
go run .
```

The Golang application will be running at `http://localhost:8080`.

## Running the Golang Application with Redis as Persistent Storage

To run the Golang application using Redis as a persistent storage, run the command:

```
docker-compose up -d
```

to initalize a redis container, then navigate to the Golang service folder and run the command:

```
go run . -useredis=true
```
